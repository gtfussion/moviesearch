import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import axios from 'axios'
import './detailPage.css'
export const MovieDetail = (props) => {
    const { match } = props
    const [movieDetail, setMovieDetail] = useState({})
    useEffect(() => {
        async function getMovieDetail() {
            try {
                const res = await axios.get(
                    `http://www.omdbapi.com/?i=${match.params.id}&apikey=17dd49da`,
                );
                setMovieDetail(res.data)
            } catch (e) {
                console.log(e);
            }
        }
        getMovieDetail()
    }, [match.params.id])
    return (
        <MovieWrapper className="wrapper" backdrop={`${movieDetail.Poster}`}>
            <div className="detail-page">
                <img className="shadow" src={`${movieDetail.Poster}`} alt={movieDetail.Title} />
                <div>
                    <h1>{movieDetail.Title}</h1>
                    <h2>{movieDetail.Genre}</h2>
                    <h5>{movieDetail.Year},{movieDetail.Writer}</h5>
                    <h5>{movieDetail.Plot}</h5>
                </div>
            </div>
        </MovieWrapper>
    );
}

const MovieWrapper = styled.div`
  position: relative;
  padding-top: 50vh;
  background: url(${props => props.backdrop}) no-repeat;
  background-size: cover;
`;
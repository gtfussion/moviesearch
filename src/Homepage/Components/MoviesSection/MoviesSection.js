import React from 'react'
import { MovieTile } from '../MovieTile/MovieTile'

export const MoviesSection = (props) => {
    const { moviesList } = props
    return (
        <div >
            {
                moviesList?.map((eachmovie, index) => (
                    <MovieTile
                        key={`${eachmovie.imdbID}${index}`}
                        movieDetail={eachmovie}
                    />
                ))
            }
        </div>
    )
}

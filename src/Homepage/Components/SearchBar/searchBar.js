import React, { useState } from 'react'
import { FaSearch } from 'react-icons/fa'
import { ErrorMsg } from '../Error/ErrorMsg'
import './searchBar.css'
export const SearchBar = (props) => {
    const [searchValue, setSearchValue] = useState('')
    const [errorMsg, setErrorMsg] = useState('')
    const onSubmit = (e) => {
        e.preventDefault();
        if (searchValue) {
            props.onSubmit(searchValue)
            setErrorMsg('')
        } else {
            setErrorMsg('Search Field Should Not be Empty')
        }
    }
    return (
        <div className="search-bar">
            <p>Search Movies...</p>
            <form onSubmit={onSubmit}>
                <div className="input-group">
                    <input value={searchValue} onChange={(e) => setSearchValue(e.target.value)} type='text'></input>
                    <button type="submit">
                        <FaSearch />
                    </button>
                </div>
            </form>
            <ErrorMsg
                Msg={errorMsg}
            />
        </div>

    )
}

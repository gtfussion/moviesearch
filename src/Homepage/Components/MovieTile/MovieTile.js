import React from 'react'
import { Link } from 'react-router-dom'
import './movieTile.css'
export const MovieTile = (props) => {
    const { movieDetail } = props
    return (
        <Link to={`/${movieDetail.imdbID}`}>
            <div className="movie-tile">
                <img src={movieDetail.Poster} alt={movieDetail.Title} />
                <div className="description">
                    <p>Title: {movieDetail.Title}</p>
                    <p>Year:{movieDetail.Year}</p>
                    <p>Type:{movieDetail.Type}</p>
                </div>
            </div>
        </Link>
    )
}

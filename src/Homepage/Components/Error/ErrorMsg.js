import React from 'react'
import './ErrorMsg.css'
export const ErrorMsg = (props) => {
    return (
        <div className=" error-msg">
            <center>{props.Msg}</center>
        </div>
    )
}

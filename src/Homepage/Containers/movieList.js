import React, { useEffect, useState } from 'react'
import { ErrorMsg } from '../Components/Error/ErrorMsg'
import { MoviesSection } from '../Components/MoviesSection/MoviesSection'
import { SearchBar } from '../Components/SearchBar/SearchBar'
import axios from 'axios'
import { url } from '../../App'
export const MovieList = () => {
    const [movies, setMovies] = useState([])
    const [errorMsg, setError] = useState('')
    useEffect(() => {
        async function getMoviesList() {
            var res = await axios.get(`${url}`)
            setError('')
            setMovies(prevMovies => [...prevMovies, res.data])
        }
        getMoviesList()
    }, [])
    const searchResult = async (searchValue) => {
        var res = await axios.get(`${url}&s=${searchValue}`)
        if (res.data.Response === "True") {
            console.log(res.data)
            setMovies(res.data.Search)
            setError('')
        } else {
            setError(res.data.Error)
            setMovies([])
        }
    }
    return (
        <div className="container">
            <SearchBar
                onSubmit={searchResult}
            />
            <ErrorMsg
                Msg={errorMsg}
            />
            <MoviesSection
                moviesList={movies}
            />
        </div>
    )
}

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { MovieDetail } from './Detailpage/MovieDetail';
import { MovieList } from './Homepage/Containers/MovieList';
import { Header } from './Header/Header'
const App = () => {

    return (
        <Router>
            <div >
                <Header />
                <Switch>
                    <Route exact path='/' component={MovieList} />
                    <Route exact path="/:id" component={MovieDetail} />
                </Switch>
            </div>
        </Router>
    )
}
export default App;
export const url = "http://www.omdbapi.com/?i=tt3896198&apikey=17dd49da"

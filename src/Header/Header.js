import React from 'react'
import './header.css'
import logo from '../photos/movie.jpg'
import { Link } from 'react-router-dom'
export const Header = () => {
    return (

        <div className="header">
            <div className="logo-section">
                <Link to='/'>
                    <img src={`${logo}`} alt="Logo" />
                </Link>
            </div>
        </div>
    )
}
